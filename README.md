<h1> Автоматизированная система для оружейного магазина "Золотой Калибр" <h1>

Продукт GoldCaliber создан для автоматизации бизнесс процессов в оружейном бизнесе. Он призван повысить эффективность и прибыльность магазина оружия.
Для более полной информации можете обратиться к ТЗ: https://gitlab.com/spectre9973737/Gold_Caliber_App/-/blob/main/%D0%A2%D0%97_%D0%BE%D1%80%D1%83%D0%B6%D0%B5%D0%B9%D0%BD%D1%8B%D0%B9_%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD.docx 

Дизайн Фигма - https://www.figma.com/file/MGYnXtHtvRlz8LCeWitjWb/GoldCalliberApp?type=design&node-id=0-1&mode=design&t=G6rbuBvoIL8eFHK3-0

![Alt text](erd.png)

![Alt text](image-2.png)

![Alt text](image.png)

![Alt text](image-3.png)

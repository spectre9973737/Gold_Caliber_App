﻿using System;
using GoldCaliberApp.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static GoldCaliberApp.ClassHelpers.Basket;

namespace GoldCaliberApp.Windows
{
    /// <summary>
    /// Interaction logic for CatalogWindow.xaml
    /// </summary>
    public partial class CatalogWindow : Window
    {
        public CatalogWindow()
        {
            InitializeComponent();

            using (GoldCaliberDbContext db = new GoldCaliberDbContext())
            {
                LvProds.ItemsSource = db.Products.ToList();
            };

        }
        private void btnToBuy_Click(object sender, RoutedEventArgs e)
        {
            BasketWindow basketWindow = new();
            basketWindow.Show();
            Close();
        }

        private void btnAddProd_Click(object sender, RoutedEventArgs e)
        {
            basket.Add((Product)LvProds.SelectedItem);
            MessageBox.Show("Товар добавлен в корзину");
        }
    }
}

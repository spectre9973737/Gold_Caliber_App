﻿using GoldCaliberApp.ClassHelpers;
using GoldCaliberApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static GoldCaliberApp.ClassHelpers.Basket;

namespace GoldCaliberApp.Windows
{
    /// <summary>
    /// Interaction logic for BasketWindow.xaml
    /// </summary>
    public partial class BasketWindow : Window
    {
        public BasketWindow()
        {
            InitializeComponent();
            LvBasket.ItemsSource = basket;
            lblTotal.Content = basket.Sum(x => x.Price) + " р.";
            
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            CatalogWindow catalogWindow = new();
            catalogWindow.Show();
            Close();
        }

        private void btnDelProd_Click(object sender, RoutedEventArgs e)
        {
            basket.Remove((Product)LvBasket.SelectedItem);
        }
    }
}

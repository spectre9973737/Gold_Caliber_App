﻿using System;
using System.Collections.Generic;

namespace GoldCaliberApp.Models;

public partial class Product
{
    public int IdProduct { get; set; }

    public string ProdName { get; set; } = null!;

    public decimal Price { get; set; }

    public int? IdCategory { get; set; }

    public int? IdAccessLevel { get; set; }

    public int? IdСonsumables { get; set; }
}
